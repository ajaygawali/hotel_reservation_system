package com.hotel.hotelreservation;

import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

import com.hotel.hotelreservation.service.HotelReservation;

@SpringBootApplication
public class HotelreservationApplication  {

	public static void main(String[] args) {
		new SpringApplicationBuilder(HotelreservationApplication.class)
				.web(WebApplicationType.NONE)
				.run(args);
		new HotelReservation().hotelBookingMain();
		
	}
	

}
