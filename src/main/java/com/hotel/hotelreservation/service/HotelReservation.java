package com.hotel.hotelreservation.service;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Service;

import com.hotel.hotelreservation.enums.CustomerType;
import com.hotel.hotelreservation.enums.Days;
import com.hotel.hotelreservation.model.HotelBooking;

@Service
public class HotelReservation {

	private static HotelBooking lakewood, bridgewood, ridgewood;

	@PostConstruct
	private void hotelsRegistry() {
		// setting values for Lakewood
		lakewood = new HotelBooking("Lakewood");
		lakewood.setRegularWeekDay(110);
		lakewood.setRewardeeWeekDay(80);
		lakewood.setRegularWeekEnd(90);
		lakewood.setRewardeeWeekEnd(80);
		lakewood.setRatings(3);

		// setting values for Bridgewood
		bridgewood = new HotelBooking("Bridgewood");
		bridgewood.setRegularWeekDay(160);
		bridgewood.setRewardeeWeekDay(110);
		bridgewood.setRegularWeekEnd(60);
		bridgewood.setRewardeeWeekEnd(50);
		bridgewood.setRatings(4);

		// setting values for Ridgewood
		ridgewood = new HotelBooking("Ridgewood");
		ridgewood.setRegularWeekDay(220);
		ridgewood.setRewardeeWeekDay(100);
		ridgewood.setRegularWeekEnd(150);
		ridgewood.setRewardeeWeekEnd(40);
		ridgewood.setRatings(5);
	}

	public  String hotelBookingMain() {

		Scanner scanner = null;
		String inputValue = "";
		try {

			scanner = new Scanner(System.in);
			System.out.print("input - ");
			inputValue = scanner.nextLine();
			return this.getCheapestHotel(inputValue);

		}catch(Exception e) {
			
			System.out.println("Something went wrong pleae try again !");
		}
		finally {
			scanner.close();
		}

		return null;
	}

	private String getCheapestHotel(String inputValue) throws Exception{

		String[] typeDateArr = inputValue.split(":");
		String type = typeDateArr[0];
		String hotelDates = typeDateArr[1];

		int cost_lakewood = 0;
		int cost_bridgewood = 0;
		int cost_ridgewood = 0;

		Matcher dayMatcher = Pattern.compile("\\((.*?)\\)").matcher(hotelDates.trim());

		while (dayMatcher.find()) {
			String day = dayMatcher.group(1);

			if (day.equalsIgnoreCase(Days.SAT.name()) || day.equalsIgnoreCase(Days.SUN.name())) {

				if (type.equalsIgnoreCase(CustomerType.REGULAR.toString())) {

					cost_lakewood += lakewood.getRegularWeekEnd();
					cost_bridgewood += bridgewood.getRegularWeekEnd();
					cost_ridgewood += ridgewood.getRegularWeekEnd();
				} else {
					cost_lakewood += lakewood.getRewardeeWeekEnd();
					cost_bridgewood += bridgewood.getRewardeeWeekEnd();
					cost_ridgewood += ridgewood.getRewardeeWeekEnd();
				}
			} else {
				if (type.equalsIgnoreCase(CustomerType.REGULAR.toString())) {
					cost_lakewood += lakewood.getRegularWeekDay();
					cost_bridgewood += bridgewood.getRegularWeekDay();
					cost_ridgewood += ridgewood.getRegularWeekDay();
				} else {
					cost_lakewood += lakewood.getRewardeeWeekDay();
					cost_bridgewood += bridgewood.getRewardeeWeekDay();
					cost_ridgewood += ridgewood.getRewardeeWeekDay();
				}
			}
		}

		lakewood.setTempCost(cost_lakewood);
		bridgewood.setTempCost(cost_bridgewood);
		ridgewood.setTempCost(cost_ridgewood);

		List<HotelBooking> hotelCostList = Arrays.asList(lakewood, bridgewood, ridgewood);

		HotelBooking cheapestHotel = findCheapestHotel(hotelCostList);

		System.out.println(cheapestHotel.getHotelName());

		return cheapestHotel.getHotelName();
	}

	private static HotelBooking findCheapestHotel(List<HotelBooking> hotelCostList) {

		Comparator<HotelBooking> sortComparator = (o1, o2) -> {
			if (o1.getTempCost() == o2.getTempCost()) {

				if (o1.getRatings() == o2.getRatings()) {
					return 0;
				}
				if (o1.getRatings() < o2.getRatings()) {
					return 1;
				} else {
					return -1;
				}
			}
			if (o1.getTempCost() > o2.getTempCost()) {
				return 1;
			} else {
				return -1;
			}
		};

//        hotelCostList.stream().sorted(sortComparator).forEach(hotel -> System.out.println(hotel.getHotelName() + " " + hotel.getTempCost()));

		Optional<HotelBooking> cheapestHotel = hotelCostList.stream().sorted(sortComparator).findFirst();

		return cheapestHotel.get();
	}

}
