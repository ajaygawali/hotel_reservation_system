package com.hotel.hotelreservation;

import static org.junit.Assert.assertEquals;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.hotel.hotelreservation.service.HotelReservation;

@RunWith(SpringRunner.class)
@SpringBootTest
class HotelreservationApplicationTests {

	@Autowired
	private HotelReservation hotelReservation;

	@Test
	public void hotelSearch1() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
		String input = " Regular: 16Mar2009(mon), 17Mar2009(tues), 18Mar2009(wed";
		Method method = HotelReservation.class.getDeclaredMethod("getCheapestHotel", String.class);
		method.setAccessible(true);
		String returnValue = (String) method.invoke(hotelReservation, input);
		assertEquals("Lakewood", returnValue);

	}

	@Test
	public void hotelSearch2() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
		String input = "Regular: 20Mar2009(fri), 21Mar2009(sat), 22Mar2009(sun)";
		Method method = HotelReservation.class.getDeclaredMethod("getCheapestHotel", String.class);
		method.setAccessible(true);
		String returnValue = (String) method.invoke(hotelReservation, input);
		assertEquals("Bridgewood", returnValue);

	}

	@Test
	public void hotelSearch3() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
		String input = "Rewards: 26Mar2009(thur), 27Mar2009(fri), 28Mar2009(sat)";
		Method method = HotelReservation.class.getDeclaredMethod("getCheapestHotel", String.class);
		method.setAccessible(true);
		String returnValue = (String) method.invoke(hotelReservation, input);
		assertEquals("Ridgewood", returnValue);

	}

}
